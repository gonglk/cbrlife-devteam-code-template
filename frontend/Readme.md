## Frontend
### 前提条件
本地自己搭建好http环境，基于http环境调试，可以用VS Code IDE的Live Server插件，也可以用node.js，随你喜好。

### 目录说明
- Views : 存放View视图html
- index.css : 自定义css内容
- index.js : 自定义js内容
- index.html : 入口html 

### 快速上手指南
CBRLife APP的前端目前使用传统的Html5 + Css + JS，使用跨平台UI框架[Framework7](https://framework7.io/docs/)，所以coding是基于F7框架提供的各种现成的Components和API，与此同时再根据需要coding自定义的css和js。<br>

假设我们要开发一个example页面，会有几个步骤: <br>
1. 在views目录下创建example页面的view: example.html<br>
2. 在index.js的new Framework7的routes参数中新增example页面的路由配置 (路由名称、路由路径、路由指向的html、监听事件)<br>
3. 对example.html开始coding，相关的js可以全部放在index.js中，相关的css可以全部放在index.css中，你不用take care代码拆分问题，我会负责。<br>
4. 最后你可以通过浏览器访问，地址最后加上 #!/你的路由名称 ， 如: http://127.0.0.1:5500/frontend/#!/example<br>

### 关于Dom的选择和操作
Framework7的Dom选择和操作和Jquery类似，基于F7的Dom7库，基本Jquery有的Dom7也有，Jquery使用 $ 作为开头，而Framework7用 $$ 作为开头，如: `$$("#example").html("HELLO WORLD!"); ` <br>
附: [Framework7 Dom API](https://framework7.io/docs/dom7.html)

### 关于AJAX的使用
使用F7框架的request方法。<br>
- 如: GET请求:<br>
```
app.request.get('somepage.html').then((res) => {
  console.log(res.data);
});
```
- 如: POST请求:<br>
```
app.request.post('auth.php', { username:'foo', password: 'bar' })
  .then(function (res) {
    $$('.login').html(res.data);
    console.log('Load was performed');
  });
```
- 如: POST请求(JSON):<br>
```
app.request.postJSON('http://api.myapp.com', { username:'foo', password: 'bar' })
  .then(function (res) {
    console.log(res.data);
  });
```
附: [F7的数据请求API](https://framework7.io/docs/request.html)