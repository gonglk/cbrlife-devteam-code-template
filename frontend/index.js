/**
 * Framework7 configuration
 * https://framework7.io/docs/app.html
 */
var $$ = Dom7;

var app = new Framework7({
  // App root element
  root: '#app',
  // App Name
  name: 'CBRLife堪生活',
  // App id
  id: 'com.cbrlife.app',
  cache: true, //todo
  theme: 'ios', //ios, md, aurora or auto
  dialog: {
    buttonOk: '好的',
    buttonCancel: "取消"
  },
  touch: {
    // Enable fast clicks
    fastClicks: true,
    tapHold: true //enable tap hold events
  },
  lazy: {
    sequential: false //如果启用，则当懒惰图像出现在视口中时，它们将被一一加载
  },
  view: {
    pushState: true,
    stackPages: true,//	如果启用，则当您进行越来越深的导航时，导航链中的所有先前页面都不会从DOM中删除。例如，如果您有5个步骤（5页）中的某些表格，而在最后5页上，则需要访问第一页上的表格，这可能会很有用。
    animate: true, //启用页面之间的过渡动画，默认true
    iosSwipeBackActiveArea: 500, //页面侧滑距离，默认30 之前250
    iosDynamicNavbar: false, //页面侧滑ios主题动态navbar，默认true
    iosSwipeBackAnimateShadow: false, //向后滑动动作期间启用/禁用框阴影动画。您可以禁用它以提高性能。对于iOS主题
    iosSwipeBackAnimateOpacity: false, //向后滑动动作期间启用/禁用不透明度动画。您可以禁用它以提高性能。对于iOS主题
    restoreScrollTopOnBack: false //启用后，当您返回此页面时，它将恢复页面滚动顶部
  },
  navbar: {
    iosCenterTitle: true
  },
  //define events
  on: {
    //when framework7 is ready
    init: function() {},
    //when page is ready (page mounted)
    pageInit: function() {}
  },
  routes: [{
    name: 'example',
    path: '/example',
    url: './views/example.html',
    on: {
      pageInit: function (e, page) {
        console.log('example page is loaded');
      }
    }
  }
  ,
  {
    name:"login",
    path:'/login',
    url:'./views/login.html'
  }
  ,
  {
    name:"register",
    path:'/register',
    url:'./views/register.html'
  }
  ,
  {
    name:"favourite",
    path:'/favourite',
    url:'./views/favourite.html'
  }
  ,
  {
    name:"history",
    path:'/history',
    url:'./views/history.html'
  }
  ,
  {
    name:"privacy",
    path:'/privacy',
    url:'./views/privacy.html'
  }
  ,
  {
    name:"setting",
    path:'/setting',
    url:'./views/setting.html'
  }
  ,
  {
    name:"help",
    path:'/help',
    url:'./views/help.html'
  }
  ,


]
});

//init framework7 view
var mainView = app.views.create('.view-main');

const exampleAlert = () => {
  app.dialog.alert('Hello');
}
var searchbar = app.searchbar.create({
  el: '.searchbar',
  on: {
    enable: function () {
      console.log('Searchbar enabled')
    }
  }
})

var swiper = app.swiper.create('.swiper-container', {
  speed: 400,
  spaceBetween: 100
});

