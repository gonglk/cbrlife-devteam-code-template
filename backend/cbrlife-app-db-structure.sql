/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table app_ad
# ------------------------------------------------------------

CREATE TABLE `app_ad` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `_id` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `adtype` int(10) DEFAULT NULL COMMENT '1 主页头部条幅 =>主页顶部轮播广告位\r\n2 信息列表中间条幅 =>信息流插播位\r\n12 子版块信息流插播位\r\n4 长图条幅 =>长图条幅广告位\r\n5 黄页顶部 =>黄页顶部轮播广告位\r\n6 详细页面 =>信息详情页底部广告位\r\n7 同城各板块顶部 =>同城子板块顶部广告位\r\n8 主页置顶位\r\n9 主页中部banner广告位\r\n10 新闻详情页底部广告位\r\n11 新闻详情页顶部广告位\r\n',
  `urltype` int(10) DEFAULT NULL COMMENT '1 指向跳转\r\n3 指向图片\r\n4 指向微信号\r\n5 无\r\n6 指向黄页\r\n7 专题\r\n8 指向APP下载页\r\n20  新闻\r\n21 堪发现帖子\r\n22 黄页主页\r\n23 堪发现主页\r\n24 同城帖子\r\n25 黄页主页\r\n26 论坛主页\r\n27 论坛帖子\r\n28 指向直播页面\r\n29 外链',
  `toWeixin_number` varchar(255) DEFAULT NULL,
  `toWeixin_notice` varchar(255) DEFAULT NULL,
  `toURL` varchar(255) DEFAULT NULL,
  `toImg` varchar(255) DEFAULT NULL,
  `toYellowPage` int(10) DEFAULT NULL,
  `toNewsPage` int(10) DEFAULT NULL,
  `toEventsPage` int(10) DEFAULT NULL,
  `toPostPage` int(10) DEFAULT NULL,
  `toForumPage` int(10) DEFAULT NULL,
  `adPic` varchar(255) DEFAULT NULL,
  `fromDate` date DEFAULT NULL,
  `toDate` date DEFAULT NULL,
  `adStatus` int(10) DEFAULT '1' COMMENT '1上架 2下架',
  `leixing` varchar(255) DEFAULT NULL,
  `shangjia` varchar(255) DEFAULT NULL,
  `which_tongcheng_subtype` int(10) DEFAULT NULL COMMENT '12 子版块信息流插播位\r\n或 \r\n7 同城子板块顶部广告位 所指定的同城子类',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`title`),
  KEY `3` (`adtype`),
  KEY `4` (`urltype`),
  KEY `5` (`toWeixin_number`),
  KEY `6` (`toWeixin_notice`),
  KEY `7` (`toURL`),
  KEY `8` (`adPic`),
  KEY `9` (`fromDate`),
  KEY `10` (`toDate`),
  KEY `11` (`adStatus`),
  KEY `12` (`toImg`),
  KEY `13` (`toYellowPage`),
  KEY `14` (`toNewsPage`),
  KEY `15` (`leixing`),
  KEY `16` (`shangjia`),
  KEY `17` (`toEventsPage`),
  KEY `18` (`toPostPage`),
  KEY `19` (`which_tongcheng_subtype`),
  KEY `20` (`toForumPage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_ad_cf
# ------------------------------------------------------------

CREATE TABLE `app_ad_cf` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adowner` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `adtitle` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `adtype` int(10) DEFAULT NULL,
  `adImg` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `adUrl` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `add_datetime` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `toWeixin_number` varchar(255) DEFAULT NULL,
  `toWeixin_notice` varchar(255) DEFAULT NULL,
  `toURL` varchar(255) DEFAULT NULL,
  `toImg` varchar(255) DEFAULT NULL,
  `toYellowPage` int(10) DEFAULT NULL,
  `toNewsPage` int(10) DEFAULT NULL,
  `toEventsPage` int(10) DEFAULT NULL,
  `toPostPage` int(10) DEFAULT NULL,
  `toForumPage` int(10) DEFAULT NULL,
  `adPic` varchar(255) DEFAULT NULL,
  `leixing` varchar(255) DEFAULT NULL,
  `shangjia` varchar(255) DEFAULT NULL,
  `which_tongcheng_subtype` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`) USING BTREE,
  KEY `2` (`adtype`) USING BTREE,
  KEY `3` (`adImg`(191)) USING BTREE,
  KEY `4` (`adUrl`(191)) USING BTREE,
  KEY `5` (`add_datetime`) USING BTREE,
  KEY `6` (`adowner`(191)) USING BTREE,
  KEY `7` (`adtitle`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;



# Dump of table app_ad_cf_config
# ------------------------------------------------------------

CREATE TABLE `app_ad_cf_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adtype` int(10) DEFAULT NULL,
  `adname` varchar(255) DEFAULT NULL,
  `value` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`adtype`),
  KEY `3` (`adname`(191)),
  KEY `4` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_ad_tracking
# ------------------------------------------------------------

CREATE TABLE `app_ad_tracking` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `adid` int(10) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1 view 2 clicked',
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `adid` (`adid`),
  KEY `datetime` (`datetime`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_admin_config
# ------------------------------------------------------------

CREATE TABLE `app_admin_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `token` varchar(255) NOT NULL DEFAULT '',
  `admin_flag` int(1) DEFAULT '0' COMMENT '0不是管理员 1是',
  `menu_list` varchar(1000) DEFAULT 'dashboard,',
  `last_login_time` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1正常 0禁用',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `name` (`name`(191)) USING BTREE,
  KEY `uname` (`username`(191)) USING BTREE,
  KEY `pwd` (`password`(191)) USING BTREE,
  KEY `llt` (`last_login_time`) USING BTREE,
  KEY `admin` (`admin_flag`) USING BTREE,
  KEY `token` (`token`(191)) USING BTREE,
  KEY `menulist` (`menu_list`(191)) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `openid` (`openid`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;



# Dump of table app_area_config
# ------------------------------------------------------------

CREATE TABLE `app_area_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `main_id` int(10) DEFAULT NULL,
  `main_name` varchar(255) DEFAULT NULL,
  `sub_id` int(10) DEFAULT NULL,
  `sub_name` varchar(255) DEFAULT NULL,
  `order_number` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`main_name`),
  KEY `3` (`main_id`),
  KEY `4` (`sub_id`),
  KEY `5` (`sub_name`),
  KEY `6` (`order_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_austalia_state_config
# ------------------------------------------------------------

CREATE TABLE `app_austalia_state_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `order_number` int(10) DEFAULT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`name`(191)),
  KEY `3` (`order_number`),
  KEY `4` (`name_en`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_car_brand_config
# ------------------------------------------------------------

CREATE TABLE `app_car_brand_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `t_id` int(10) DEFAULT NULL,
  `t_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`t_id`),
  KEY `3` (`t_name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_changtu_datetime_config
# ------------------------------------------------------------

CREATE TABLE `app_changtu_datetime_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_changtu_top_banner_config
# ------------------------------------------------------------

CREATE TABLE `app_changtu_top_banner_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`name`(191)),
  KEY `3` (`url`(191)),
  KEY `4` (`start_time`),
  KEY `5` (`end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_data_analysis
# ------------------------------------------------------------

CREATE TABLE `app_data_analysis` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ana_type` varchar(255) DEFAULT NULL,
  `ana_name` varchar(255) DEFAULT NULL,
  `ana_value` int(10) DEFAULT '1',
  `ana_datetime` datetime DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`ana_type`(191)),
  KEY `3` (`ana_name`(191)),
  KEY `4` (`ana_value`),
  KEY `5` (`ana_datetime`),
  KEY `6` (`comments`(191)),
  KEY `7` (`ip`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_event_reply
# ------------------------------------------------------------

CREATE TABLE `app_event_reply` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `content` text,
  `reply_count` int(10) DEFAULT '0',
  `zan_count` int(10) DEFAULT '0',
  `reply_which` int(10) DEFAULT '0',
  `reply_main_id` int(10) DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`openid`(191)),
  KEY `4` (`reply_count`),
  KEY `5` (`zan_count`),
  KEY `6` (`reply_which`),
  KEY `7` (`post_date`),
  KEY `8` (`reply_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_events
# ------------------------------------------------------------

CREATE TABLE `app_events` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `desc` text,
  `start` datetime DEFAULT NULL,
  `cost` varchar(10) DEFAULT NULL,
  `weixin` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `shoucang` int(10) DEFAULT '0',
  `like` int(10) DEFAULT '0',
  `zhuanfa` int(10) DEFAULT '0',
  `openid` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `setTop` int(1) DEFAULT '0' COMMENT '0不置顶 1置顶',
  `setTop_start` date DEFAULT NULL,
  `setTop_end` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`title`(191)),
  KEY `3` (`start`),
  KEY `4` (`cost`),
  KEY `5` (`weixin`(191)),
  KEY `6` (`tel`(191)),
  KEY `7` (`openid`(191)),
  KEY `8` (`datetime`),
  KEY `9` (`shoucang`),
  KEY `10` (`like`),
  KEY `11` (`zhuanfa`),
  KEY `12` (`openid`(191)),
  KEY `13` (`datetime`),
  KEY `14` (`setTop`),
  KEY `15` (`setTop_start`),
  KEY `16` (`setTop_end`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_events_baoming
# ------------------------------------------------------------

CREATE TABLE `app_events_baoming` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `event_id` int(10) DEFAULT NULL,
  `baoming` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `weixin` varchar(255) DEFAULT NULL,
  `notice_status` int(10) DEFAULT '0' COMMENT '0未推送 1已推送',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`event_id`),
  KEY `3` (`baoming`(191)),
  KEY `4` (`datetime`),
  KEY `5` (`weixin`(191)),
  KEY `6` (`tel`(191)),
  KEY `7` (`notice_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_events_comments
# ------------------------------------------------------------

CREATE TABLE `app_events_comments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `c_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`comments`(191)),
  KEY `4` (`openid`(191)),
  KEY `5` (`c_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_events_img
# ------------------------------------------------------------

CREATE TABLE `app_events_img` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `events_id` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`events_id`),
  KEY `3` (`url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_events_video
# ------------------------------------------------------------

CREATE TABLE `app_events_video` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `event_id` int(10) DEFAULT NULL,
  `video_url` varchar(255) NOT NULL,
  `suoluetu_url` varchar(255) DEFAULT NULL,
  `m3u8_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`event_id`),
  KEY `3` (`video_url`(191)),
  KEY `4` (`suoluetu_url`(191)),
  KEY `5` (`m3u8_url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_exchange_rate
# ------------------------------------------------------------

CREATE TABLE `app_exchange_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`value`),
  KEY `3` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_forum_dianzan
# ------------------------------------------------------------

CREATE TABLE `app_forum_dianzan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `reply_id` int(10) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`reply_id`),
  KEY `3` (`openid`(191)),
  KEY `4` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_forum_list_config
# ------------------------------------------------------------

CREATE TABLE `app_forum_list_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_forum_post
# ------------------------------------------------------------

CREATE TABLE `app_forum_post` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `list_id` int(10) DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `desc` text,
  `openid` varchar(255) DEFAULT NULL,
  `visit_count` int(10) DEFAULT '0',
  `reply_count` int(10) DEFAULT '0',
  `post_date` datetime DEFAULT NULL,
  `setTop` int(10) DEFAULT '0' COMMENT '0不置顶 1置顶',
  `setTop_start` date DEFAULT NULL,
  `setTop_end` date DEFAULT NULL,
  `IndexTop` int(10) DEFAULT '0' COMMENT '0主页不置顶 0主页置顶',
  `IndexTop_start` date DEFAULT NULL,
  `IndexTop_end` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`list_id`),
  KEY `3` (`title`(191)),
  KEY `4` (`desc`(191)),
  KEY `5` (`openid`(191)),
  KEY `6` (`visit_count`),
  KEY `7` (`reply_count`),
  KEY `8` (`post_date`),
  KEY `9` (`setTop`),
  KEY `10` (`setTop_start`),
  KEY `11` (`setTop_end`),
  KEY `12` (`IndexTop`),
  KEY `13` (`IndexTop_start`),
  KEY `14` (`IndexTop_end`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_forum_post_img
# ------------------------------------------------------------

CREATE TABLE `app_forum_post_img` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_forum_post_reply
# ------------------------------------------------------------

CREATE TABLE `app_forum_post_reply` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `content` text,
  `reply_count` int(10) DEFAULT '0',
  `zan_count` int(10) DEFAULT '0',
  `reply_which` int(10) DEFAULT '0',
  `reply_main_id` int(10) DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `type` int(10) DEFAULT '1' COMMENT '1同城 2活动',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`openid`(191)),
  KEY `4` (`reply_count`),
  KEY `5` (`zan_count`),
  KEY `6` (`reply_which`),
  KEY `7` (`post_date`),
  KEY `8` (`reply_main_id`),
  KEY `9` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_house_rent_cycle_config
# ------------------------------------------------------------

CREATE TABLE `app_house_rent_cycle_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cycle_id` int(10) DEFAULT NULL,
  `cycle_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`cycle_id`),
  KEY `3` (`cycle_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_house_rent_method_config
# ------------------------------------------------------------

CREATE TABLE `app_house_rent_method_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `method_id` int(10) DEFAULT NULL,
  `method_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`method_id`),
  KEY `3` (`method_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_house_type_config
# ------------------------------------------------------------

CREATE TABLE `app_house_type_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type_id` int(10) DEFAULT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`type_id`),
  KEY `3` (`type_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_im
# ------------------------------------------------------------

CREATE TABLE `app_im` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `session_id` int(10) DEFAULT NULL,
  `from` varchar(255) DEFAULT NULL COMMENT '系统消息填所属功能（如：订单提醒、回帖提醒），用户消息填写发送消息的用户的openid',
  `to` varchar(300) DEFAULT NULL COMMENT '接收方用户openid',
  `type` int(10) DEFAULT NULL COMMENT '1系统消息 2用户消息',
  `stuff_id` int(10) DEFAULT NULL COMMENT '相关外链的id，如帖子、论坛等',
  `message` varchar(1000) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `datetime_timestamp` varchar(255) DEFAULT NULL COMMENT '标准时间戳UTC',
  `notice_status` int(10) DEFAULT '0' COMMENT '0未提醒 1已提醒',
  `read_status` int(10) DEFAULT '0' COMMENT '0未读 1已读',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`to`(191)),
  KEY `3` (`type`),
  KEY `4` (`from`(191)),
  KEY `5` (`message`(191)),
  KEY `6` (`datetime`),
  KEY `7` (`notice_status`),
  KEY `8` (`read_status`),
  KEY `9` (`stuff_id`),
  KEY `10` (`datetime_timestamp`(191)),
  KEY `11` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_im_blacklist
# ------------------------------------------------------------

CREATE TABLE `app_im_blacklist` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `openid` varchar(300) DEFAULT NULL,
  `blacklist_openid` varchar(300) DEFAULT NULL COMMENT '黑名单',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`openid`(191)),
  KEY `3` (`blacklist_openid`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_im_friend
# ------------------------------------------------------------

CREATE TABLE `app_im_friend` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `openid` varchar(300) DEFAULT NULL,
  `friend` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`openid`(191)),
  KEY `3` (`friend`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_im_images
# ------------------------------------------------------------

CREATE TABLE `app_im_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `im_id` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`im_id`),
  KEY `3` (`url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_im_notice_type_config
# ------------------------------------------------------------

CREATE TABLE `app_im_notice_type_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pre_text` varchar(255) DEFAULT NULL COMMENT '改类型通知的消息前缀语',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`name`(191)),
  KEY `3` (`pre_text`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_im_sessions
# ------------------------------------------------------------

CREATE TABLE `app_im_sessions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`users`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_im_user_session
# ------------------------------------------------------------

CREATE TABLE `app_im_user_session` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `session_id` int(10) DEFAULT NULL,
  `user_id` varchar(300) DEFAULT NULL,
  `hide_status` int(10) DEFAULT '0' COMMENT '默认0不隐藏，1为隐藏',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`session_id`),
  KEY `3` (`user_id`(191)),
  KEY `4` (`hide_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_init_screen
# ------------------------------------------------------------

CREATE TABLE `app_init_screen` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `desc` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `jump_type` int(10) DEFAULT NULL COMMENT '1:帖子 2:黄页 3:论坛 4:活动 5:专题 100:堪发现主页 101:堪发现帖子 102:新闻',
  `jump_value` int(10) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `app` int(10) DEFAULT '1' COMMENT 'app是否显示',
  `mini` int(10) DEFAULT '1' COMMENT '小程序是否显示',
  `wailian_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`desc`(191)),
  KEY `3` (`url`(191)),
  KEY `4` (`from_date`),
  KEY `5` (`to_date`),
  KEY `6` (`create_time`),
  KEY `7` (`jump_type`),
  KEY `8` (`jump_value`),
  KEY `9` (`app`),
  KEY `10` (`mini`),
  KEY `11` (`wailian_url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_ip_country_log
# ------------------------------------------------------------

CREATE TABLE `app_ip_country_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`ip`(191)),
  KEY `3` (`country`(191)),
  KEY `4` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_ip2location
# ------------------------------------------------------------

CREATE TABLE `app_ip2location` (
  `ip_from` int(10) unsigned DEFAULT NULL,
  `ip_to` int(10) unsigned DEFAULT NULL,
  `country_code` char(2) DEFAULT NULL,
  `country_name` varchar(64) DEFAULT NULL,
  KEY `idx_ip_from` (`ip_from`),
  KEY `idx_ip_to` (`ip_to`),
  KEY `idx_ip_from_to` (`ip_from`,`ip_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_live_list
# ------------------------------------------------------------

CREATE TABLE `app_live_list` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(20) NOT NULL,
  `cate_name_2` varchar(20) DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `subname` varchar(500) DEFAULT NULL,
  `status` int(10) NOT NULL COMMENT '0 未开始直播 1直播中 2已结束',
  `content` text,
  `pic` varchar(255) NOT NULL,
  `hls` int(10) DEFAULT '0' COMMENT '0嵌入式 1非嵌入式(hls)',
  `start_time` datetime NOT NULL,
  `is_live` int(10) NOT NULL DEFAULT '1' COMMENT '1是 0不是直播',
  `datetime` datetime NOT NULL,
  `laiyuan` varchar(100) NOT NULL,
  `org_url` varchar(255) DEFAULT NULL COMMENT '原站连接',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`name`(191)),
  KEY `3` (`status`),
  KEY `4` (`pic`(191)),
  KEY `5` (`start_time`),
  KEY `6` (`datetime`),
  KEY `7` (`hls`),
  KEY `8` (`cate_name`),
  KEY `9` (`is_live`),
  KEY `10` (`laiyuan`),
  KEY `12` (`cate_name_2`),
  KEY `13` (`org_url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_live_status
# ------------------------------------------------------------

CREATE TABLE `app_live_status` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `status` int(10) NOT NULL COMMENT '0维护中 1正常',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_menu_config
# ------------------------------------------------------------

CREATE TABLE `app_menu_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order` int(10) DEFAULT NULL,
  `name_on_menu` varchar(255) DEFAULT NULL,
  `name_on_tab` varchar(255) DEFAULT NULL,
  `link_id` varchar(255) DEFAULT NULL,
  `type_id` int(10) DEFAULT NULL,
  `logo_url` varchar(255) DEFAULT NULL,
  `display_status` int(10) DEFAULT '1' COMMENT '1显示 0隐藏',
  `tab_status` int(10) DEFAULT '1' COMMENT '1显示 0隐藏',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`order`),
  KEY `3` (`name_on_menu`(191)),
  KEY `4` (`name_on_tab`(191)),
  KEY `5` (`link_id`(191)),
  KEY `6` (`logo_url`(191)),
  KEY `7` (`display_status`),
  KEY `8` (`type_id`),
  KEY `9` (`tab_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_message_push
# ------------------------------------------------------------

CREATE TABLE `app_message_push` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT '0' COMMENT '0未推送 1已推送',
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`openid`(191)),
  KEY `3` (`title`(191)),
  KEY `4` (`content`(191)),
  KEY `5` (`status`),
  KEY `6` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_news
# ------------------------------------------------------------

CREATE TABLE `app_news` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `source` int(10) DEFAULT NULL COMMENT '1:自编辑 2:微信公众号 3:外部URL',
  `jianshu` varchar(255) DEFAULT NULL COMMENT '新闻简述',
  `content` varchar(255) DEFAULT NULL COMMENT '地址或连接',
  `news_content` longtext COMMENT '新闻内容',
  `pic_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(10) DEFAULT '1' COMMENT '1:上线 2:下线',
  `setTop` int(10) DEFAULT '0' COMMENT '0:非置顶 1:置顶',
  `setTop_start` date DEFAULT NULL,
  `setTop_end` date DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `mid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `click_count` int(10) DEFAULT '0',
  `reply_count` int(10) DEFAULT '0',
  `share_count` int(10) DEFAULT '0',
  `news_fenlei` int(10) DEFAULT '1',
  `news_zuozhe` varchar(255) DEFAULT NULL,
  `news_tuijian` int(10) DEFAULT '0' COMMENT '0未推荐 1已推荐',
  `news_tuijian_order` int(10) DEFAULT '0' COMMENT '推荐新闻排序',
  `bianji_type` varchar(255) DEFAULT NULL,
  `image_count` int(10) DEFAULT '0',
  `zhuanzai_desc` varchar(255) DEFAULT NULL,
  `create_user` varchar(255) DEFAULT NULL,
  `yuanwenlianjie` varchar(355) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`title`),
  KEY `3` (`source`),
  KEY `4` (`status`),
  KEY `5` (`setTop`),
  KEY `6` (`setTop_start`),
  KEY `7` (`setTop_end`),
  KEY `8` (`datetime`),
  KEY `9` (`pic_url`),
  KEY `10` (`content`(191)),
  KEY `11` (`owner`(191)),
  KEY `12` (`click_count`),
  KEY `13` (`reply_count`),
  KEY `14` (`news_fenlei`),
  KEY `15` (`news_zuozhe`(191)),
  KEY `16` (`news_tuijian`),
  KEY `17` (`news_tuijian_order`),
  KEY `18` (`jianshu`(191)),
  KEY `19` (`update_time`),
  KEY `20` (`bianji_type`(191)),
  KEY `21` (`image_count`),
  KEY `22` (`create_user`(191)),
  KEY `23` (`share_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_news_baoliao_list
# ------------------------------------------------------------

CREATE TABLE `app_news_baoliao_list` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `baoliao_content` text,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `contact_weixin` varchar(255) DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `chuli_status` int(10) DEFAULT '0' COMMENT '0未处理 1已处理',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_date`),
  KEY `3` (`contact_name`(191)),
  KEY `4` (`contact_number`(191)),
  KEY `5` (`contact_weixin`(191)),
  KEY `6` (`chuli_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_news_baoliao_list_img
# ------------------------------------------------------------

CREATE TABLE `app_news_baoliao_list_img` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `baoliao_id` int(10) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`baoliao_id`) COMMENT ' ',
  KEY `3` (`img_url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_news_comments
# ------------------------------------------------------------

CREATE TABLE `app_news_comments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `news_id` int(10) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `comments` text,
  `post_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`news_id`),
  KEY `3` (`user_id`(191)),
  KEY `5` (`post_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_news_image
# ------------------------------------------------------------

CREATE TABLE `app_news_image` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `news_id` int(10) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`news_id`),
  KEY `3` (`url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_news_push
# ------------------------------------------------------------

CREATE TABLE `app_news_push` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `news_id` int(10) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`news_id`),
  KEY `3` (`openid`(191)),
  KEY `4` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_news_suoluetu
# ------------------------------------------------------------

CREATE TABLE `app_news_suoluetu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `news_id` int(10) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`news_id`),
  KEY `3` (`img`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_news_top_swiper_config
# ------------------------------------------------------------

CREATE TABLE `app_news_top_swiper_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `news_id` int(10) DEFAULT NULL,
  `news_title` varchar(255) DEFAULT NULL,
  `logo_url` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`news_id`),
  KEY `3` (`start_date`),
  KEY `4` (`end_date`),
  KEY `5` (`logo_url`(191)),
  KEY `6` (`news_title`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_news_type_config
# ------------------------------------------------------------

CREATE TABLE `app_news_type_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`type_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_post
# ------------------------------------------------------------

CREATE TABLE `app_post` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `m_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `maintype` int(10) DEFAULT NULL,
  `subtype` int(10) DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `tel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `weixin` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `isTop` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `topFromDate` date DEFAULT NULL,
  `topToDate` date DEFAULT NULL,
  `topOrder` int(10) DEFAULT '99',
  `clickCount` int(10) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `changtu_status` int(10) DEFAULT '0' COMMENT '0未生成过长图 1已生成长图',
  `keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`m_id`),
  KEY `3` (`maintype`),
  KEY `4` (`subtype`),
  KEY `5` (`openid`),
  KEY `6` (`title`(191)),
  KEY `8` (`tel`),
  KEY `9` (`weixin`),
  KEY `10` (`isTop`),
  KEY `11` (`topFromDate`),
  KEY `12` (`topToDate`),
  KEY `13` (`date`),
  KEY `14` (`clickCount`),
  KEY `15` (`changtu_status`),
  KEY `16` (`keywords`(191)),
  KEY `17` (`topOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_post_car_infos
# ------------------------------------------------------------

CREATE TABLE `app_post_car_infos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `m_id` varchar(255) DEFAULT NULL,
  `post_id` int(10) DEFAULT NULL,
  `brand_id` int(10) DEFAULT NULL,
  `year` int(10) DEFAULT NULL,
  `km` int(10) DEFAULT NULL,
  `price` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`m_id`(191)),
  KEY `3` (`post_id`),
  KEY `4` (`brand_id`),
  KEY `5` (`year`),
  KEY `6` (`km`),
  KEY `7` (`price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_post_comments
# ------------------------------------------------------------

CREATE TABLE `app_post_comments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `c_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`comments`(191)),
  KEY `4` (`openid`(191)),
  KEY `5` (`c_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_post_img_size_log
# ------------------------------------------------------------

CREATE TABLE `app_post_img_size_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `pic_size` varchar(100) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`pic_size`),
  KEY `4` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_post_pic
# ------------------------------------------------------------

CREATE TABLE `app_post_pic` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `_id` varchar(255) DEFAULT NULL,
  `pics_0_path` varchar(255) DEFAULT NULL,
  `pics_1_path` varchar(255) DEFAULT NULL,
  `pics_2_path` varchar(255) DEFAULT NULL,
  `pics_3_path` varchar(255) DEFAULT NULL,
  `pics_4_path` varchar(255) DEFAULT NULL,
  `pics_5_path` varchar(255) DEFAULT NULL,
  `pics_6_path` varchar(255) DEFAULT NULL,
  `pics_7_path` varchar(255) DEFAULT NULL,
  `pics_8_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`_id`),
  KEY `4` (`pics_0_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_post_pictures
# ------------------------------------------------------------

CREATE TABLE `app_post_pictures` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `slt` varchar(255) DEFAULT NULL COMMENT '缩略图',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`url`),
  KEY `4` (`slt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_post_pingche
# ------------------------------------------------------------

CREATE TABLE `app_post_pingche` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `m_id` varchar(255) DEFAULT NULL,
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `seat` int(10) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`m_id`),
  KEY `4` (`from`),
  KEY `5` (`to`),
  KEY `7` (`start_time`),
  KEY `8` (`seat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_post_setTop
# ------------------------------------------------------------

CREATE TABLE `app_post_setTop` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `_id` varchar(255) DEFAULT NULL,
  `index_page` varchar(10) DEFAULT NULL,
  `all_page` varchar(10) DEFAULT NULL,
  `sort_order` int(10) DEFAULT NULL,
  `index_page_order` int(10) DEFAULT '999',
  `all_page_order` int(10) DEFAULT '999',
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`_id`),
  KEY `3` (`index_page`),
  KEY `4` (`all_page`),
  KEY `5` (`sort_order`),
  KEY `6` (`from_date`),
  KEY `7` (`to_date`),
  KEY `8` (`post_id`),
  KEY `9` (`index_page_order`),
  KEY `10` (`all_page_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_post_speed_track
# ------------------------------------------------------------

CREATE TABLE `app_post_speed_track` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `finish` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`start`),
  KEY `3` (`finish`),
  KEY `4` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_post_tend
# ------------------------------------------------------------

CREATE TABLE `app_post_tend` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) DEFAULT NULL,
  `_id` varchar(255) DEFAULT NULL,
  `areaMain` int(10) DEFAULT NULL,
  `areaSub` int(10) DEFAULT NULL,
  `houseType` int(10) DEFAULT NULL,
  `methodType` int(10) DEFAULT NULL,
  `cycleType` int(10) DEFAULT NULL,
  `tendPay` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`post_id`),
  KEY `3` (`_id`),
  KEY `4` (`areaSub`),
  KEY `5` (`houseType`),
  KEY `6` (`methodType`),
  KEY `7` (`cycleType`),
  KEY `8` (`tendPay`),
  KEY `9` (`areaMain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_post_type_config
# ------------------------------------------------------------

CREATE TABLE `app_post_type_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `main_type_id` int(10) DEFAULT NULL,
  `main_type_name` varchar(255) DEFAULT NULL,
  `sub_type_id` int(10) DEFAULT NULL,
  `sub_type_name` varchar(255) DEFAULT NULL,
  `template_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`main_type_id`),
  KEY `3` (`main_type_name`),
  KEY `4` (`sub_type_id`),
  KEY `5` (`sub_type_name`),
  KEY `6` (`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_public_address_geo
# ------------------------------------------------------------

CREATE TABLE `app_public_address_geo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `address` varchar(500) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL COMMENT '纬度',
  `lng` varchar(255) DEFAULT NULL COMMENT '经度',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`address`(191)),
  KEY `3` (`lat`(191)),
  KEY `4` (`lng`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_sms_send_log
# ------------------------------------------------------------

CREATE TABLE `app_sms_send_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sms_number` varchar(255) DEFAULT NULL,
  `sms_text` varchar(255) DEFAULT NULL,
  `sms_type` int(255) DEFAULT NULL COMMENT '1外卖订单提醒',
  `comments` varchar(500) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`sms_number`(191)),
  KEY `3` (`sms_text`(191)),
  KEY `4` (`sms_type`),
  KEY `5` (`comments`(191)),
  KEY `6` (`send_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_sms_validcode
# ------------------------------------------------------------

CREATE TABLE `app_sms_validcode` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `validcode` varchar(4) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`phone`(191)),
  KEY `3` (`validcode`),
  KEY `4` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_subject_2019_covid_data
# ------------------------------------------------------------

CREATE TABLE `app_subject_2019_covid_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `siwang` int(10) DEFAULT NULL,
  `kangfu` int(10) DEFAULT NULL,
  `laiyuan` varchar(255) DEFAULT NULL,
  `beizhu` varchar(255) DEFAULT NULL,
  `update_time` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`date`),
  KEY `4` (`siwang`),
  KEY `5` (`kangfu`),
  KEY `7` (`laiyuan`(191)),
  KEY `8` (`beizhu`(191)),
  KEY `9` (`update_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_subject_2019_covid_data_act_data
# ------------------------------------------------------------

CREATE TABLE `app_subject_2019_covid_data_act_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `time` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`time`(191)),
  KEY `3` (`text`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_subject_2019_covid_data_details
# ------------------------------------------------------------

CREATE TABLE `app_subject_2019_covid_data_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `where` int(10) DEFAULT NULL,
  `quezhen` int(10) DEFAULT NULL,
  `siwang` int(10) DEFAULT NULL,
  `laiyuan` varchar(255) DEFAULT NULL,
  `beizhu` varchar(255) DEFAULT NULL,
  `update_time` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`date`),
  KEY `3` (`where`),
  KEY `4` (`quezhen`),
  KEY `5` (`siwang`),
  KEY `6` (`laiyuan`(191)),
  KEY `7` (`beizhu`(191)),
  KEY `8` (`update_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_subject_2019_covid_data_more_details
# ------------------------------------------------------------

CREATE TABLE `app_subject_2019_covid_data_more_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`title`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_subject_2019_covid_data_new_updates
# ------------------------------------------------------------

CREATE TABLE `app_subject_2019_covid_data_new_updates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(255) DEFAULT NULL,
  `infos` varchar(255) DEFAULT NULL,
  `day` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`state_name`(191)),
  KEY `3` (`infos`(191)),
  KEY `4` (`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_subject_2019_covid_data_timeline
# ------------------------------------------------------------

CREATE TABLE `app_subject_2019_covid_data_timeline` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `where` int(10) DEFAULT NULL,
  `quezhen_date` date DEFAULT NULL,
  `age` int(10) DEFAULT NULL,
  `sex` int(10) DEFAULT NULL COMMENT '1男 2女',
  `beizhu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`where`),
  KEY `3` (`quezhen_date`),
  KEY `4` (`age`),
  KEY `5` (`sex`),
  KEY `6` (`beizhu`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_subject_2019_covid_data_visit_count
# ------------------------------------------------------------

CREATE TABLE `app_subject_2019_covid_data_visit_count` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `from` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `3` (`datetime`),
  KEY `4` (`from`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_temp
# ------------------------------------------------------------

CREATE TABLE `app_temp` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `v` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_user
# ------------------------------------------------------------

CREATE TABLE `app_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `channel` int(1) DEFAULT '1' COMMENT '1:微信 2:web 3:手机',
  `openid` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `nickname` varchar(30) DEFAULT NULL,
  `nickname_app` varchar(30) DEFAULT NULL,
  `headnav` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `admin_flag` int(1) DEFAULT '0' COMMENT '1:管理员 0:非管理员',
  `passwd` varchar(255) DEFAULT NULL,
  `areacode` int(5) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `notph` int(1) DEFAULT NULL,
  `notPhFromDate` date DEFAULT NULL,
  `notPhToDate` date DEFAULT NULL,
  `notPhRemark` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `last_login_datetime` datetime DEFAULT NULL,
  `notice_status` int(10) DEFAULT '0' COMMENT ' 是否接收消息推送提醒，默认1接收 0关闭',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `1` (`id`),
  KEY `2` (`channel`),
  KEY `3` (`openid`),
  KEY `4` (`nickname`),
  KEY `5` (`headnav`),
  KEY `6` (`admin_flag`),
  KEY `8` (`notph`),
  KEY `9` (`notPhFromDate`),
  KEY `10` (`notPhToDate`),
  KEY `11` (`notPhRemark`),
  KEY `12` (`nickname_app`),
  KEY `13` (`last_login_datetime`),
  KEY `14` (`notice_status`),
  KEY `15` (`passwd`(191)),
  KEY `16` (`areacode`),
  KEY `17` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_user_favorites
# ------------------------------------------------------------

CREATE TABLE `app_user_favorites` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` int(10) DEFAULT '1' COMMENT '1帖子 2黄页 3活动',
  `post_id` int(10) DEFAULT NULL,
  `m_id` varchar(255) DEFAULT NULL,
  `hy_id` int(10) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`type`),
  KEY `3` (`post_id`),
  KEY `4` (`m_id`(191)),
  KEY `5` (`hy_id`),
  KEY `6` (`openid`(191)),
  KEY `7` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_user_history
# ------------------------------------------------------------

CREATE TABLE `app_user_history` (
  `id` int(10) NOT NULL,
  `device` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`device`(191)),
  KEY `3` (`user`(191)),
  KEY `4` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_user_jpush_regid
# ------------------------------------------------------------

CREATE TABLE `app_user_jpush_regid` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `regid` varchar(255) DEFAULT NULL COMMENT 'jpush注册/推送id',
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`openid`(191)),
  KEY `3` (`regid`(191)),
  KEY `4` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_user_likes
# ------------------------------------------------------------

CREATE TABLE `app_user_likes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` int(10) DEFAULT NULL COMMENT '1帖子 2黄页 3活动',
  `post_id` int(10) DEFAULT NULL,
  `openid` varchar(300) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`type`),
  KEY `3` (`post_id`),
  KEY `4` (`openid`(191)),
  KEY `5` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_waimai_food
# ------------------------------------------------------------

CREATE TABLE `app_waimai_food` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sid` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `price` varchar(10) DEFAULT NULL,
  `miaoshu` varchar(255) DEFAULT NULL,
  `logo_url` varchar(300) DEFAULT NULL,
  `logo_base64` text,
  `fenlei` varchar(255) DEFAULT '默认分类',
  `status` int(10) DEFAULT '1' COMMENT '1上架中 2下架中',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`sid`),
  KEY `3` (`title`(191)),
  KEY `4` (`price`),
  KEY `5` (`miaoshu`(191)),
  KEY `6` (`logo_url`(191)),
  KEY `7` (`fenlei`(191)),
  KEY `8` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_waimai_food_fenlei
# ------------------------------------------------------------

CREATE TABLE `app_waimai_food_fenlei` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shopid` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`shopid`),
  KEY `3` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_waimai_order
# ------------------------------------------------------------

CREATE TABLE `app_waimai_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order_user_id` varchar(255) DEFAULT NULL,
  `total_price` varchar(10) DEFAULT NULL,
  `food_price` varchar(10) DEFAULT NULL,
  `peisong_price` varchar(10) DEFAULT NULL,
  `order_shop_name` varchar(255) DEFAULT NULL,
  `order_shop_id` int(10) DEFAULT NULL,
  `ps_lianxiren` varchar(255) DEFAULT NULL,
  `ps_lianxidianhua` varchar(255) DEFAULT NULL,
  `ps_peisongdizhi` varchar(255) DEFAULT NULL,
  `beizhu` varchar(255) DEFAULT NULL,
  `order_status` int(10) DEFAULT '0' COMMENT '0订单已创建,等待店铺确认 1店铺已确认,正在准备餐品 2配送中 3已完成 (系统自动将超过1天的2状态改为3状态) 4已取消',
  `order_notice_status` int(10) DEFAULT '0' COMMENT '0未提醒 1已提醒',
  `order_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`order_user_id`(191)),
  KEY `3` (`total_price`),
  KEY `4` (`order_shop_name`(191)),
  KEY `5` (`order_shop_id`),
  KEY `6` (`ps_lianxiren`(191)),
  KEY `7` (`ps_lianxidianhua`(191)),
  KEY `8` (`ps_peisongdizhi`(191)),
  KEY `9` (`beizhu`(191)),
  KEY `10` (`order_status`),
  KEY `11` (`order_notice_status`),
  KEY `12` (`order_datetime`),
  KEY `13` (`food_price`),
  KEY `14` (`peisong_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_waimai_order_details
# ------------------------------------------------------------

CREATE TABLE `app_waimai_order_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(10) DEFAULT NULL,
  `food_name` varchar(255) DEFAULT NULL,
  `food_danjia` varchar(255) DEFAULT NULL,
  `food_count` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`order_id`),
  KEY `3` (`food_name`(191)),
  KEY `4` (`food_danjia`(191)),
  KEY `5` (`food_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_waimai_region_config
# ------------------------------------------------------------

CREATE TABLE `app_waimai_region_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `region_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`region_name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_waimai_shop
# ------------------------------------------------------------

CREATE TABLE `app_waimai_shop` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `caixi` varchar(255) DEFAULT NULL,
  `yingye_day` varchar(255) DEFAULT NULL,
  `yingye_shijian` varchar(255) DEFAULT NULL,
  `qisongjiage` varchar(255) DEFAULT NULL,
  `renjunxiaofei` varchar(255) DEFAULT NULL,
  `peisongquyu` varchar(255) DEFAULT NULL COMMENT '如果全部区域,则是all，反之则是区域名字逗号分隔',
  `peisongfei` varchar(255) DEFAULT NULL,
  `peisongshijian` int(10) DEFAULT NULL COMMENT '1联系店主 2随点随送',
  `beizhu` varchar(500) DEFAULT NULL,
  `c_name` varchar(255) DEFAULT NULL,
  `c_shouji` varchar(255) DEFAULT NULL,
  `c_weixin` varchar(255) DEFAULT NULL,
  `logo_url` varchar(300) DEFAULT NULL,
  `logo_base64` text,
  `join_date` datetime DEFAULT NULL,
  `join_user` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`title`(191)),
  KEY `3` (`address`(191)),
  KEY `4` (`caixi`(191)),
  KEY `5` (`yingye_shijian`(191)),
  KEY `6` (`qisongjiage`(191)),
  KEY `7` (`renjunxiaofei`(191)),
  KEY `8` (`peisongfei`(191)),
  KEY `9` (`peisongshijian`),
  KEY `10` (`beizhu`(191)),
  KEY `11` (`c_name`(191)),
  KEY `12` (`c_shouji`(191)),
  KEY `13` (`c_weixin`(191)),
  KEY `14` (`logo_url`(191)),
  KEY `15` (`join_date`),
  KEY `16` (`join_user`(191)),
  KEY `17` (`yingye_day`(191)),
  KEY `18` (`peisongquyu`(191)),
  KEY `19` (`longitude`(191)),
  KEY `20` (`latitude`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_weather
# ------------------------------------------------------------

CREATE TABLE `app_weather` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `temp_min` varchar(255) DEFAULT NULL,
  `temp_max` varchar(255) DEFAULT NULL,
  `main` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`city`),
  KEY `3` (`temp_min`),
  KEY `4` (`temp_max`),
  KEY `5` (`main`),
  KEY `6` (`date`),
  KEY `7` (`icon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_woyaotuiguang
# ------------------------------------------------------------

CREATE TABLE `app_woyaotuiguang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `xm` varchar(30) DEFAULT NULL,
  `hangye` varchar(30) DEFAULT NULL,
  `dianhua` varchar(30) DEFAULT NULL,
  `weixin` varchar(30) DEFAULT NULL,
  `youxiang` varchar(50) DEFAULT NULL,
  `beizhu` varchar(200) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '0' COMMENT '0默认已提交 1已处理',
  `mail_status` int(10) DEFAULT '0' COMMENT '0未发邮件 1已发',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`datetime`),
  KEY `3` (`status`),
  KEY `4` (`mail_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_woyaotuiguang_hy
# ------------------------------------------------------------

CREATE TABLE `app_woyaotuiguang_hy` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `xm` varchar(30) DEFAULT NULL,
  `hangye` varchar(30) DEFAULT NULL,
  `dianhua` varchar(30) DEFAULT NULL,
  `weixin` varchar(30) DEFAULT NULL,
  `youxiang` varchar(50) DEFAULT NULL,
  `beizhu` varchar(200) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '0' COMMENT '0默认已提交 1已处理',
  `mail_status` int(10) DEFAULT '0' COMMENT '0未发邮件 1已发',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`datetime`),
  KEY `3` (`status`),
  KEY `4` (`mail_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_yellow
# ------------------------------------------------------------

CREATE TABLE `app_yellow` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `m_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `maintype` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `tel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `weixin` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `renjun_xiaofei` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `youhui` text CHARACTER SET utf8,
  `yingye_day` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `yingye_time_from` time DEFAULT NULL,
  `yingye_time_to` time DEFAULT NULL,
  `isTop` varchar(10) CHARACTER SET utf8 DEFAULT 'false',
  `isTop_from` date DEFAULT '2000-01-01',
  `isTop_to` date DEFAULT '2000-01-01',
  `isTop_type` int(10) DEFAULT NULL COMMENT '空代表全部',
  `isTop_order` int(10) DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `visit_count` int(10) DEFAULT NULL,
  `shenhe_status` int(10) DEFAULT '0' COMMENT '0未审核 1已审核',
  `renzheng_status` int(10) DEFAULT '0' COMMENT '0未认证 1已认证',
  `renzheng_start` date DEFAULT NULL,
  `renzheng_end` date DEFAULT NULL,
  `renzheng_index_show` int(11) DEFAULT '1' COMMENT '1在主页认证区显示，0不显示',
  `renzheng_fenlei_show` int(11) DEFAULT '1' COMMENT '1在分类认证区显示，0不显示',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`maintype`),
  KEY `3` (`title`(191)),
  KEY `4` (`address`),
  KEY `5` (`tel`),
  KEY `6` (`weixin`),
  KEY `7` (`renjun_xiaofei`),
  KEY `9` (`yingye_time_from`),
  KEY `10` (`yingye_time_to`),
  KEY `11` (`isTop`),
  KEY `12` (`isTop_from`),
  KEY `13` (`isTop_to`),
  KEY `14` (`openid`),
  KEY `15` (`post_date`),
  KEY `16` (`yingye_day`),
  KEY `17` (`isTop_type`),
  KEY `18` (`isTop_order`),
  KEY `19` (`visit_count`),
  KEY `20` (`shenhe_status`),
  KEY `21` (`renzheng_status`),
  KEY `22` (`renzheng_start`),
  KEY `23` (`renzheng_end`),
  KEY `m_id` (`m_id`),
  KEY `renzheng_index_show` (`renzheng_index_show`),
  KEY `renzheng_fenlei_show` (`renzheng_fenlei_show`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_yellow_modify
# ------------------------------------------------------------

CREATE TABLE `app_yellow_modify` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `yp_id` int(10) DEFAULT NULL,
  `m_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `maintype` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `tel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `weixin` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `renjun_xiaofei` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `youhui` text CHARACTER SET utf8,
  `yingye_day` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `yingye_time_from` time DEFAULT NULL,
  `yingye_time_to` time DEFAULT NULL,
  `isTop` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `isTop_from` date DEFAULT NULL,
  `isTop_to` date DEFAULT NULL,
  `isTop_type` int(10) DEFAULT NULL COMMENT '空代表全部',
  `isTop_order` int(10) DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `visit_count` int(10) DEFAULT NULL,
  `shenhe_status` int(10) DEFAULT '1' COMMENT '0未审核 1已审核',
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`maintype`),
  KEY `3` (`title`(191)),
  KEY `4` (`address`),
  KEY `5` (`tel`),
  KEY `6` (`weixin`),
  KEY `7` (`renjun_xiaofei`),
  KEY `9` (`yingye_time_from`),
  KEY `10` (`yingye_time_to`),
  KEY `11` (`isTop`),
  KEY `12` (`isTop_from`),
  KEY `13` (`isTop_to`),
  KEY `14` (`openid`),
  KEY `15` (`post_date`),
  KEY `16` (`yingye_day`),
  KEY `17` (`isTop_type`),
  KEY `18` (`isTop_order`),
  KEY `19` (`visit_count`),
  KEY `20` (`shenhe_status`),
  KEY `21` (`yp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table app_yellow_pic
# ------------------------------------------------------------

CREATE TABLE `app_yellow_pic` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `m_id` varchar(255) DEFAULT NULL,
  `y_id` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`m_id`),
  KEY `3` (`y_id`),
  KEY `4` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_yellow_pic_modify
# ------------------------------------------------------------

CREATE TABLE `app_yellow_pic_modify` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `m_id` varchar(255) DEFAULT NULL,
  `y_id` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`m_id`),
  KEY `3` (`y_id`),
  KEY `4` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_yellow_pic_products
# ------------------------------------------------------------

CREATE TABLE `app_yellow_pic_products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `m_id` varchar(255) DEFAULT NULL,
  `y_id` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`m_id`),
  KEY `3` (`y_id`),
  KEY `4` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_yellow_pic_products_modify
# ------------------------------------------------------------

CREATE TABLE `app_yellow_pic_products_modify` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `m_id` varchar(255) DEFAULT NULL,
  `y_id` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`m_id`),
  KEY `3` (`y_id`),
  KEY `4` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_yellow_subtype
# ------------------------------------------------------------

CREATE TABLE `app_yellow_subtype` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `m_id` varchar(255) DEFAULT NULL,
  `y_id` int(10) DEFAULT NULL,
  `subtype` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`m_id`),
  KEY `3` (`y_id`),
  KEY `4` (`subtype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_yellow_subtype_modify
# ------------------------------------------------------------

CREATE TABLE `app_yellow_subtype_modify` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `m_id` varchar(255) DEFAULT NULL,
  `y_id` int(10) DEFAULT NULL,
  `subtype` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`m_id`),
  KEY `3` (`y_id`),
  KEY `4` (`subtype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table app_yellow_type_config
# ------------------------------------------------------------

CREATE TABLE `app_yellow_type_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `main_id` int(10) DEFAULT NULL,
  `_id` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `1` (`id`),
  KEY `2` (`_id`),
  KEY `3` (`name`),
  KEY `4` (`main_id`),
  KEY `5` (`logo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
