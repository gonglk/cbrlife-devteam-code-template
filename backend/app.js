const compression = require('compression')
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const fs = require('fs');
const port = 3000;
const app = express();

//配置gzip compression
app.use(compression());

// CORS模块，处理web端跨域问题
app.use(cors());

//body-parser 解析表单
//配置最大传输内容（上传文件）大小
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.post('/', function(req, res) {
    res.send("CBRLIFE PRIVATE APIs, Welcome!");
});
app.get('/', function(req, res) {
    res.send("CBRLIFE PRIVATE APIs, Welcome!");
});

/**
 * 引入各JS接口
 */

//新闻接口
const api_example = require('./routes/example/example');
app.use('/example', api_example);//define route url for example api


//特殊事件处理，如404和500
app.use(function(req, res) { //处理404页面
    res.type('text/plain');
    res.status(404);
    res.json({
        code: 1,
        message: '404-这个地址丢了!'
    });
});
app.use(function(err, req, res, next) { //定制500页面（错误处理）
    console.error(err.stack);
    res.type('text/plain');
    res.status(500);
    res.json({
        code: 1,
        message: '接口500错误, 错误内容: ' + err
    });
});

//启动API
const httpsServer = http.createServer(app);
httpsServer.listen(port, () => console.log(`Listening on port ${port}!`))
