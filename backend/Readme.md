# backend
## 前提条件
- 本地安装最新版的node.js
- 本地安装mysql环境，创建cbrlife_app数据库（编码: utf8mb4 + utf8mb4_general_ci）*备注: 表类型为InnoDB*
- 将cbrlife-app-db-structure.sql (backend文件夹根目录下) 导入cbrlife_app数据库

## 概述
backend api使用node.js + express框架，coding语言是javascript，通过express的router()方法，为每个接口文件配置不同的接口地址。<br>
开发模式是本地开发，使用你已经创建好的cbrlife_app数据库进行本地调试

## 使用指南
- 完成 前提条件
- fork本repo，在本地进入到backend目录下，然后执行 `npm install` 命令安装package依赖
- 修改api_config.json中的数据库连接配置（配置文件中读和写要单独配置，本地调试的时候读写配置都填一样的即可，为什么会这样？因为线上是读写分离的）

假如需要开发一个新的接口，接口名称是example，应该是什么步骤呢？<br>

1. 在backend/routes目录下，创建一个example目录，然后创建example.js（这是这个接口的入口文件），然后再创建exampleAPI.js（这个是这个接口的具体实现逻辑）
2. 在exampleAPI.js中，编写一个接口GetInfos，每个接口必须把接口上面的comments写清楚（这个接口的作用、请求URL、请求参数、返回信息（可选））. 传过来的参数，可以通过 req.body.xxxx获取，xxxx就是接口的参数，比如要获取传过来的id，那么可以用 req.body.id
3. 在exampleAPI.js的GetInfos接口中，使用dbpool(数据库连接池)对数据库进行CURD操作，对于数据查询，使用queryRead(), 对于数据写入，使用queryWrite()
