/**
 * example API entrance
 */

const express = require('express');
const router = express.Router();

router.post('/', (req, res) => {
    return res.json({
        code: 1,
        message: '根/不处理API请求'
    });

});


const exampleAPIs = require("./exampleAPI");
router.use("/exampleInfos", exampleAPIs); //full url: http://localhost:xxxx/example/exampleInfos/


module.exports = router;