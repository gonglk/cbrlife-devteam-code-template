const queryRead = require("../dbpool/dbpoolRead");
const queryWrite = require("../dbpool/dbpoolWrite");

/** API 内容**/

/**
 * 获取xxxxx /example/GetInfos
 * 
 * 请求
 |id |是  |int |示例id    |
 */
router.post('/GetInfos', (req, res) => {

  //针对必传的参数进行判断
  if (typeof (req.body.id) == 'undefined' || req.body.id == null || req.body.id == '') {
    return res.json({
        code: 1,
        message: '接口请求参数有误! [id]'
    });
  }

  //Use db pool to read data
  const sql = "select count(1) as c from app_user where id = ?";
  queryRead(sql, [req.body.id], (err, results) => {
    //if db connection failed
    if (err) {
      return res.json({
        code: 1,
        message: 'SQL错误: ' + err
      })
    }
    res.json({
      code: 200,
      message: results
    })
  });

  //Use db pool to write data
  const sql2 = "update app_user set status = false where id = ?";
  queryWrite(sql2, [req.body.id], (err, results) => {
    //if db connection failed
    if (err) {
      return res.json({
        code: 1,
        message: 'SQL错误: ' + err
      })
    }
    res.json({
      code: 200,
      message: 'Success'
    })
  });


});