const api_config = require('../../api_config.json'); //引入全局API配置文件
var mysql = require("mysql");  

var poolWrite = mysql.createPool({  
    host: api_config.OPERATE_MYSQL_HOST,
    user: api_config.OPERATE_MYSQL_USER,
    password: api_config.OPERATE_MYSQL_PASSWORD,
    port: api_config.OPERATE_MYSQL_PORT,
    database: api_config.OPERATE_MYSQL_DATABASE,
    charset: 'utf8mb4',
    dateStrings: true
});  
  
var queryWrite=function(sql,options,callback){  
  poolWrite.getConnection(function(err,conn){  
    if(err){  
      callback(err,null,null);  
    }else{  
      conn.query(sql,options,function(err,results,fields){  
        //释放连接  
        conn.release();  
        //事件驱动回调  
        callback(err,results,fields);  
      });  
    }  
  }); 
};  
  
module.exports=queryWrite;