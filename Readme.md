# CBRLife Dev-Team

## 内部沟通
使用Slack作为内部沟通工具，请勿在微信中发送工作相关消息。

[Download Slack](https://slack.com/intl/en-ca/downloads)

## 周会
堪培拉时间每周二11:00 am，我们通过Zoom进行周例会，30分钟时间，周会用于总结、规划、讨论工作。

Zoom Link: [https://us04web.zoom.us/j/9132794757?pwd=SmsyYWhRdm1SWmZ4cWJDbVBDZUlIZz09](https://us04web.zoom.us/j/9132794757?pwd=SmsyYWhRdm1SWmZ4cWJDbVBDZUlIZz09)

Meeting ID: 913 279 4757

Passcode: cbrlife

## Code IDE:
推荐使用 VS Code，其他的IDE也均可，不做特别要求

## Code 检查
自己的IDE中必须安装对应的代码检查工具，JavaScript使用ESLint，PHP使用Code Sniffer

## 代码仓库
使用[Bitbucket](https://bitbucket.org/)进行代码管理

## 协作流程
1. 登录[Bitbucket](https://bitbucket.org/)
2. Fork Repo, [https://bitbucket.org/cbrlife-devteam/cbrlife-devteam-code-template/src/master/](https://bitbucket.org/cbrlife-devteam/cbrlife-devteam-code-template/src/master/)
3. Git clone到本地
3. 本地开发，然后commit到自己的repo

## 任务看板
1. 登录[Jira](https://team-1614028623297.atlassian.net/)可以查看所有的任务情况 (用cbrlife.com.au的邮箱和密码登录)
2. 如果有新的任务分配给你，Jira会邮件通知你
