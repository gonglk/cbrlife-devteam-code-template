/**
 * version: 1.0.1
 * **/
var PUBLIC_REMOTE_REQUEST_URL="./";

$(function() {
    /**
     * 创建可复用元素
     * **/
    create_public_element_LOADING();
    create_public_element_NOTICE();
    create_public_element_notice_alert();

    //login
    var from_mini = getURLSearchString('v');
    var uname = getURLSearchString('uname');
    var upass = getURLSearchString('upass');
    var ukey = getURLSearchString('ukey');
    if(from_mini==="mini"){
        var this_chk_rsl = check_user_login_infos(from_mini,uname,upass,ukey);
        if(this_chk_rsl.result==1){
            CookieUtil.set("uname",uname);
            gotopage('index');
        }else{
            show_notice('身份认证失败,请重试!');
        }
    }

    //index
    $('#index-tab-main a[href="#tab-data"]').on('click', function (e) {
        e.preventDefault();
        page_data_get_infos('tab-data');
        $(this).tab('show');
        remove_all_tab_active('tab-data');
    });
    $('#index-tab-main a[href="#tab-staff"]').on('click', function (e) {
        e.preventDefault();
        page_staff_get_infos();
        $(this).tab('show');
        remove_all_tab_active('tab-staff');
    });
    $('#index-tab-main a[href="#tab-ad"]').on('click', function (e) {
        e.preventDefault();
        page_ad_get_infos();
        $(this).tab('show');
        remove_all_tab_active('tab-ad');
    });
    $('#index-tab-main a[href="#tab-init-screen"]').on('click', function (e) {
        e.preventDefault();
        page_is_get_infos();
        $(this).tab('show');
        remove_all_tab_active('tab-init-screen');
    });
    $('#index-tab-main a[href="#tab-news"]').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show');
        remove_all_tab_active('tab-news');
    });
    $('#index-tab-main a[href="#tab-set-top"]').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show');
        remove_all_tab_active('tab-set-top');
    });
    $('#index-tab-main a[href="#tab-yellowpage"]').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show');
        remove_all_tab_active('tab-yellowpage');
    });
    $('#index-tab-main a[href="#tab-changtu"]').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show');
        remove_all_tab_active('tab-changtu');
        ct_get_tab();
    });
    $('#index-tab-main a[href="#tab-user"]').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show');
        remove_all_tab_active('tab-user');
    });
    $('#index-tab-main a[href="#tab-info"]').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show');
        remove_all_tab_active('tab-info');
    });

});


function remove_all_tab_active(this_obj){
    $(".nav-link-index").each(function(){
        $(this).removeClass('active');
    });
    $('#index-tab-main a[href="#'+this_obj+'"]').addClass('active');
    $('.collapse').collapse('hide');
}

function gotopage(url){
    //window.location.href = url;
    window.location.replace(url);
}

//在url里搜索参数
function getURLSearchString(key) {
    var this_url = window.location.href;
    var this_index=this_url.lastIndexOf("?");
    var this_url_filter=this_url.substring(this_index+1,this_url.length);

    //var search = window.location.search;//canberraport.com/details/1/titile/?a=1&b=2
    //var str = search;
    //var str = this_url_filter.substring(1, this_url_filter.length); // 获取URL中?之后的字符（去掉第一位的问号）
    // 以&分隔字符串，获得类似name=xiaoli这样的元素数组
    var arr = this_url_filter.split("&");
    var obj = new Object();
    // 将每一个数组元素以=分隔并赋给obj对象
    for (var i = 0; i < arr.length; i++) {
        var tmp_arr = arr[i].split("=");
        obj[decodeURIComponent(tmp_arr[0])] = decodeURIComponent(tmp_arr[1]);
    }
    return obj[key];
}

function show_loading(){
    $('#loading').modal('show');
}

function hide_loading(){
    $('#loading').modal('hide');
}

function submit_login(){
    var from = "web";
    var uname = $('#username').val();
    var upass = $('#password').val();
    var ukey = $('#token').val();

    if(uname==null||uname==''){show_notice('请输入用户名');return;}
    if(upass==null||upass==''){show_notice('请输入密码');return;}
    if(ukey==null||ukey==''){show_notice('请输入密钥');return;}

    var this_chk_rsl = check_user_login_infos(from,uname,upass,ukey);
    if(this_chk_rsl.result==1){
        CookieUtil.set("uname",uname);
        gotopage('index');
    }else{
        show_notice('身份认证失败,请重试!');
    }
}

function check_user_login_infos(from,uname,upass,ukey){
    var rsl;
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        dataType: 'json',
        async: false,//同步
        data :{
            f:"check_user_infos",
            from:from,
            uname:uname,
            upass:upass,
            ukey:ukey,
            country:"au"
        },
        success:function(data){
            rsl = data;
        },
        complete : function(XMLHttpRequest,status){
            if(status == 'timeout') {}
        }
    });
    return rsl;
}

function create_public_element_LOADING(){
    $('body').append("<div class=\"modal fade\" id=\"loading\" data-backdrop=\"static\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n" +
        "    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n" +
        "        <div class=\"modal-content\">\n" +
        "            <div class=\"modal-body\">\n" +
        "                <div class=\"text-center\">\n" +
        "                    <div class=\"spinner-border\" role=\"status\">\n" +
        "                        <span class=\"sr-only\">Loading...</span>\n" +
        "                    </div>\n" +
        "                </div>\n" +
        "            </div>\n" +
        "        </div>\n" +
        "    </div>\n" +
        "</div>");
}

function create_public_element_NOTICE(){
    $('body').append("<div class=\"modal fade\" id=\"notice\" data-backdrop=\"static\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n" +
        "    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n" +
        "        <div class=\"modal-content\">\n" +
        "           <div class=\"modal-header\">\n" +
        "               <h5 class=\"modal-title\">提示</h5>\n" +
        "               <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n" +
        "               <span aria-hidden=\"true\">&times;</span>\n" +
        "               </button>\n" +
        "           </div>\n" +
        "           <div class=\"modal-body\">\n" +
        "                <p id='notice_text'></p>"+
        "           </div>\n" +
        "           <div class=\"modal-footer\">\n" +
        "               <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">关闭</button>\n" +
        "           </div>\n" +
        "        </div>\n" +
        "    </div>\n" +
        "</div>");
}

function show_notice(text){
    $('#notice_text').html(text);
    $('#notice').modal('show');
}

function page_data_get_infos(page){
    var uname = CookieUtil.get("uname");
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        dataType: 'json',
        data :{
            f:"page_data_get_infos",
            uname:uname,
            page:page
        },
        success:function(data){
            var quanxian = data.quanxian;
            if(quanxian=="no"){
                $('#tab-data-main-div').html("<div style='margin-top:200px;text-align: center;color:#1B1B1B'>您没有权限浏览该页面</div>");
                return;
            }
            $('#page_data_output_user_count').html(data.user);//
            $('#page_data_output_huodong_count').html(data.huodong_count);//
            $('#page_data_output_huodong_baoming').html(data.huodong_baoming);//
            $('#page_data_output_luntan_count').html(data.luntan_count);
            $('#page_data_output_luntan_huitie').html(data.luntan_huitie);
            $('#page_data_output_huangye_count').html(data.huangye_count);

            var tongcheng_rsl;
            var tongcheng_rsl_total=0;
            for(var i=0;i<data.tongcheng.length;i++){
                var this_rsl = data.tongcheng[i];
                var this_rsl_name = this_rsl['name'];
                var this_rsl_value = this_rsl['value'];
                tongcheng_rsl = tongcheng_rsl + "<tr><td>"+this_rsl_name+"</td><td>"+this_rsl_value+"</td></tr>";
                tongcheng_rsl_total=tongcheng_rsl_total+Number(this_rsl_value);
            }
            tongcheng_rsl = "<tr><td>全部</td><td>"+tongcheng_rsl_total+"</td></tr>" + tongcheng_rsl;
            $('#page_data_output_tongcheng').html(tongcheng_rsl);
        },
        complete : function(XMLHttpRequest,status){}
    });
}

var CookieUtil = {
    // 设置cookie
    set : function (name, value, expires, domain, path, secure) {
        var cookieText = "";
        cookieText += encodeURIComponent(name) + "=" + encodeURIComponent(value);

        cookieText += "; expires=Fri, 31 Dec 9999 23:59:59 GMT";

        if (path) {
            cookieText += "; path=" + path;
        }
        if (domain) {
            cookieText += "; domain=" + domain;
        }
        if (secure) {
            cookieText += "; secure";
        }
        document.cookie = cookieText;
    },
    // 获取cookie
    get : function (name) {
        var cookieName = encodeURIComponent(name) + "=",
            cookieStart = document.cookie.indexOf(cookieName),
            cookieValue = "";
        if (cookieStart > -1) {
            var cookieEnd = document.cookie.indexOf (";", cookieStart);
            if (cookieEnd == -1) {
                cookieEnd = document.cookie.length;
            }
            cookieValue = decodeURIComponent(document.cookie.substring(cookieStart + cookieName.length, cookieEnd));
        }
        return cookieValue;
    },
    // 删除cookie
    unset : function (name, domain, path, secure) {
        this.set(name, "", Date(0), domain, path, secure);
    }
};

function page_staff_get_infos(){
    var uname = CookieUtil.get("uname");
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"page_staff_get_infos",
            uname:uname,
            page:'tab-staff'
        },
        success:function(data){
            if(data==="no"){
                $('#page_staff_output_nodata').html("<div style='margin-top:200px;text-align: center;color:#1B1B1B'>您没有权限浏览该页面</div>");
                $('#page_staff_output_nodata').show();
                $('#page_staff_output_list_main_div').hide();
                return;
            }

            $('#page_staff_output_nodata').hide();
            $('#page_staff_output_list_main_div').show();
            $('#page_staff_output_list').html(data);
        },
        complete : function(XMLHttpRequest,status){}
    });
}

function open_add_staff_modal(type,uname,pageauth,admin,id){
    $('#add_staff_type').html(type);
    $('#add_staff_id').html(id);
    $('#add_staff_username').val(uname);
    $('#add_staff_admin').val(admin);
    if(pageauth==null||pageauth==''||typeof(pageauth)=='undefined'){
        $("#add_staff_pageauth").val([]);
    }else{
        pageauth = pageauth.split(',');
        $("#add_staff_pageauth").val(pageauth);
    }

    if(admin==null||admin==''||typeof(admin)=='undefined'){
        $('#add_staff_admin').val(0);
    }

    $('#add_staff_modal').modal('show');
}

function submit_add_staff(){
    var type = $('#add_staff_type').html();
    var id = $('#add_staff_id').html();

    var uname = $('#add_staff_username').val();
    var upass = $('#add_staff_password').val();
    var token = $('#add_staff_token').val();
    var pageauth = $('#add_staff_pageauth').val();
    var admin = $('#add_staff_admin').val();

    if(uname==null||uname==''||uname.length==0){alert("请输入用户名");return;}
    if(upass==null||upass==''||upass.length==0){alert("请输入密码");return;}
    if(token==null||token==''||token.length==0){alert("请输入密钥");return;}
    if(pageauth==null||pageauth==''||pageauth.length==0){alert("请选择页面权限");return;}

    pageauth=pageauth.join(',');

    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"submit_add_staff",
            type:type,
            id:id,
            uname:uname,
            upass:upass,
            token:token,
            pageauth:pageauth,
            admin:admin
        },
        success:function(data){
            $('#add_staff_modal').modal('hide');
            page_staff_get_infos();
        },
        complete : function(XMLHttpRequest,status){}
    });

}

function disable_staff(id){
    if(id==10){
        alert("无法禁用超级管理员账号");
        return;
    }
    var msg = "您确定要禁用这个账号吗?";
    if (confirm(msg)==true){
        $.ajax({
            url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
            timeout : 60000,
            type : 'post',
            data :{
                f:"disable_staff",
                id:id
            },
            success:function(data){
                alert("已禁用");
                page_staff_get_infos();
            },
            complete : function(XMLHttpRequest,status){}
        });
    }else{
        return false;
    }
}

function show_tab_ad_urldetails(text){
    $('#tab_ad_urldetails_rsl').html(text);
    $('#tab_ad_urldetails').modal('show');
}

var page_ad_jq_table_init = $('#table_ad').DataTable( {
    "pageLength": 100,
    "order": []
} );

function page_ad_get_infos(){
    var uname = CookieUtil.get("uname");

    page_ad_jq_table_init.destroy();

    show_loading();
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"page_ad_get_infos",
            uname:uname,
            page:'tab-ad'
        },
        success:function(data){
            hide_loading();
            if(data==="no"){
                $('#page_ad_output_nodata').html("<div style='margin-top:200px;text-align: center;color:#1B1B1B'>您没有权限浏览该页面</div>");
                $('#page_ad_output_nodata').show();
                $('#page_ad_output_list_main_div').hide();
                return;
            }

            $('#page_ad_output_nodata').hide();
            $('#page_ad_output_list_main_div').show();
            $('#page_ad_output_list').html(data);

            page_ad_jq_table_init = $('#table_ad').DataTable( {
                "pageLength": 100,
                "order": []
            } );
        },
        complete : function(XMLHttpRequest,status){}
    });
}

function page_ad_xiajia(id){
    var msg = "您确定要下架该广告吗?";
    if (confirm(msg)==true){
        $.ajax({
            url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
            timeout : 60000,
            type : 'post',
            data :{
                f:"page_ad_xiajia",
                id:id
            },
            success:function(data){
                alert("已下架");
                page_ad_get_infos();
            },
            complete : function(XMLHttpRequest,status){}
        });
    }else{
        return false;
    }
}

function page_ad_shangjia(id){
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"page_ad_shangjia",
            id:id
        },
        success:function(data){
            alert("已上架");
            page_ad_get_infos();
        },
        complete : function(XMLHttpRequest,status){}
    });
}

function show_add_ad_modal(type,id,title,adtype,urltype,weixin,weixin_notice,yellowpage_id,start,end,toImg_path,adImg_path){
    $('#add_ad_type').html(type);
    if(type=='new'){
        page_ad_img_base64='';
        $('#add_ad_ad_toImg').val('');
        page_ad_img_urltype_to_img_base64='';
        $('#add_ad_ad_img').val('');

        $('#add_ad_title').val(null);
        $('#add_ad_adtype').val('none');
        $('#add_ad_urltype').val(5);
        $('#add_ad_toWeiXin').val(null);
        $('#add_ad_toWeiXin_notice').val('微信复制成功');
        $(".after_urltype").each(function(){
            $(this).hide();
        });
        $('#add_ad_yellowpage_search_rsl').html(null);
        $('#add_ad_start').val(null);
        $('#add_ad_end').val(null);
        $("#add_ad_ad_toImg_preview").removeAttr('src');
        $("#add_ad_ad_img_preview").removeAttr('src');
    }else{
        $('#add_ad_id').html(id);
        $('#add_ad_title').val(title);
        $('#add_ad_adtype').val(adtype);
        $('#add_ad_urltype').val(urltype);
        $('#add_ad_toWeiXin').val(weixin);
        $('#add_ad_toWeiXin_notice').val(weixin_notice);
        page_ad_seach_yellowpage(yellowpage_id);
        $('#add_ad_start').val(start);
        $('#add_ad_end').val(end);
        $("#add_ad_ad_toImg_preview").attr('src',toImg_path);
        page_ad_img_urltype_to_img_base64 = toImg_path;
        $("#add_ad_ad_img_preview").attr('src',adImg_path);
        page_ad_img_base64 = adImg_path;

        $(".after_urltype").each(function(){
            $(this).hide();
        });

        if(urltype==5){}else{
            $('#after_urltype_'+urltype).show();
        }

    }

    $('#add_ad_modal').modal('show');
}

function page_ad_choose_urltype(){
    var id = $('#add_ad_urltype').val();
    $(".after_urltype").each(function(){
        $(this).hide();
    });
    if(id==5){}else{
        $('#after_urltype_'+id).show();
    }
}

var page_ad_img_base64;
function page_ad_img_afer_choose(obj){
    for(var i=0;i<obj.files.length;i++){
        var obj_file = obj.files[i];
        page_ad_img_afer_choose_change(obj_file);
    }
}

function page_ad_img_afer_choose_change(obj_file){
    //var obj_file = obj.files[0];
    var reader = new FileReader();
    var obj_file_to_base64;
    reader.addEventListener("load", function () {
        // convert image file to base64 string
        obj_file_to_base64 = reader.result;
        page_ad_img_base64 = obj_file_to_base64;
        $("#add_ad_ad_img_preview").attr('src',page_ad_img_base64);
    }, false);

    if (obj_file) {
        reader.readAsDataURL(obj_file);
    }
}

var page_ad_img_urltype_to_img_base64;
function page_ad_img_url_type_to_img_afer_choose(obj){
    for(var i=0;i<obj.files.length;i++){
        var obj_file = obj.files[i];
        page_ad_img_url_type_to_img_afer_choose_change(obj_file);
    }
}

function page_ad_img_url_type_to_img_afer_choose_change(obj_file){
    //var obj_file = obj.files[0];
    var reader = new FileReader();
    var obj_file_to_base64;
    reader.addEventListener("load", function () {
        // convert image file to base64 string
        obj_file_to_base64 = reader.result;
        page_ad_img_urltype_to_img_base64 = obj_file_to_base64;
        $("#add_ad_ad_toImg_preview").attr('src',page_ad_img_urltype_to_img_base64);
    }, false);

    if (obj_file) {
        reader.readAsDataURL(obj_file);
    }
}

function submit_add_ad(){
    var type = $('#add_ad_type').html();
    var id = $('#add_ad_id').html();
    var title = $('#add_ad_title').val();
    if(title==''||title==null||title.length==0||typeof(title)=='undefined'){alert('请填写广告描述');return;}
    var adtype = $('#add_ad_adtype').val();
    if(adtype==''||adtype==null||adtype.length==0||typeof(adtype)=='undefined'||adtype=='none'){alert('请选择广告类型');return;}
    var urltype = $('#add_ad_urltype').val();
    var urltype_to_img = page_ad_img_urltype_to_img_base64;
    var toWeixin = $('#add_ad_toWeiXin').val();
    var toWeixin_notice = $('#add_ad_toWeiXin_notice').val();
    var urltype_yellowpage_id = $('input[name="urltype_yellowpage_seachresult_radio"]').filter(':checked').val();

    if(urltype==3){
        if(urltype_to_img==null||urltype_to_img==''||urltype_to_img.length==0){
            alert('请选择指向图片');
            return;
        }
    }
    if(urltype==4){
        if(toWeixin==null||toWeixin==''||toWeixin.length==0){
            alert('请填写指向微信号');
            return;
        }
        if(toWeixin_notice==null||toWeixin_notice==''||toWeixin_notice.length==0){
            alert('请填写复制微信号后的提示');
            return;
        }
    }
    if(urltype==6){
        if(urltype_yellowpage_id==null||urltype_yellowpage_id==''||urltype_yellowpage_id.length==0||typeof(urltype_yellowpage_id)=='undefined'){
            alert('请选择指向的黄页');
            return;
        }
    }
    var time_start = $('#add_ad_start').val();
    if(time_start==''||time_start==null||time_start.length==0||typeof(time_start)=='undefined'){alert('请选择开始日期');return;}
    var time_end = $('#add_ad_end').val();
    if(time_end==''||time_end==null||time_end.length==0||typeof(time_end)=='undefined'){alert('请选择结束日期');return;}
    var adimg = page_ad_img_base64;
    if(adimg==null||adimg==''||adimg.length==0){alert('请选择广告图');return;}

    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"submit_add_ad",
            type:type,
            id:id,
            title:title,
            adtype:adtype,
            urltype:urltype,
            urltype_to_img:urltype_to_img,
            toWeixin:toWeixin,
            toWeixin_notice:toWeixin_notice,
            urltype_yellowpage_id:urltype_yellowpage_id,
            time_start:time_start,
            time_end:time_end,
            adimg:adimg
        },
        success:function(data){
            $('#add_ad_modal').modal('hide');
            alert('已添加或修改');
            page_ad_get_infos();
        },
        complete : function(XMLHttpRequest,status){}
    });

}

function page_ad_seach_yellowpage(push){
    var key = $('#add_ad_yellowpage_seach').val();
    if(key.length<2){}else{
        $.ajax({
            url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
            timeout : 60000,
            type : 'post',
            data :{
                f:"page_ad_seach_yellowpage",
                key:key
            },
            success:function(data){
                $('#add_ad_yellowpage_search_rsl').html(data);
            },
            complete : function(XMLHttpRequest,status){}
        });
    }

    if(push==null||push==''||typeof(push)=='undefined'){}else{
        $.ajax({
            url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
            timeout : 60000,
            type : 'post',
            data :{
                f:"page_ad_seach_yellowpage_id",
                key:push
            },
            success:function(data){
                $('#add_ad_yellowpage_search_rsl').html(data);
            },
            complete : function(XMLHttpRequest,status){}
        });
    }
}

function create_changtu(i,main,sub,this_time_loop_rsl,zhaozu_flag){
    $('#changtu_rsl_div_here').show();
    $('#changtu_rsl_div_here').html('');
    $('#changtu_rsl_img_here').html('');
    $('body,html').animate(
        {
            scrollTop:0
        }
    );
    if(zhaozu_flag==null||zhaozu_flag==''||typeof(zhaozu_flag)=='undefined'){zhaozu_flag=0;}
    setTimeout("create_changtu_callback("+i+","+main+","+sub+",'"+this_time_loop_rsl+"',"+zhaozu_flag+")",500);
}

var timer1;
function create_changtu_callback(i,main,sub,this_time_loop_rsl,zhaozu_flag){
    $('#notice_alert').modal('show');
    $('#notice_alert_text').html("<p align='center' style='color:red;'>正在处理长图<br>第 "+i+" 张<br>请稍后<br>!请勿关闭!</p>");

    $('#ct_preview_modal').modal('show');
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"create_changtu_grp",//create_changtu
            main:main,
            sub:sub,
            zhaozu_flag:zhaozu_flag
        },
        success:function(data){
            //$('#'+id).hide();
            $('#changtu_rsl_div_here').html(data);
            timer1 = setInterval("check_changtu_rsl_div('"+this_time_loop_rsl+"')",1000);//检测内容渲染结果

            console.log("正在创建长图:"+i+",Main:"+main+",Sub:"+sub);
        },
        complete : function(XMLHttpRequest,status){}
    });
}

var ct_last_rsl=0;
function check_changtu_rsl_div(this_time_loop_rsl){
    var rsl = $('#changtu_rsl_div_here').html().length;
    console.log("本次长图div渲染结果:"+rsl+",上一次结果:"+ct_last_rsl);
    if(rsl==null||rsl==''||rsl==0){}else{

        if(rsl==ct_last_rsl){
            ct_last_rsl=0;
            clearInterval(timer1);
            output_canvas(this_time_loop_rsl);
        }

    }
    ct_last_rsl=rsl;
}

function output_canvas(this_time_loop_rsl){
    console.log('开始输出canvas');
    html2canvas(document.querySelector("#changtu_rsl_div_here"),{
        allowTaint : false,
        scrollX: 0,
        scrollY: -window.scrollY
    }).then(canvas => {
        $('#changtu_rsl_img_here').html(canvas);
        $('#changtu_rsl_div_here').hide();

        var canvas_png = canvas.toDataURL("image/png");

        get_base64_to_img_v2(canvas_png,this_time_loop_rsl);
        //var a = document.createElement('a');
        // toDataURL defaults to png, so we need to request a jpeg, then convert for file download.
        //a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
        //a.download = 'changtu.jpg';
        //a.click();

        public_ct_create_all_done_or_not=1;//用于全部生成，代表这一次已完成

    });
}

var dtask = null;
function get_base64_to_img(v){
    var id = randomNum(0,9999);
    $.ajax({
        url:'https://app.canberraport.com/_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            "f":"api_base64_image_content",
            "content":v,
            "path":"/mnt/cbrlife_app/_upload/user_post",
            "id":id
        },
        success:function(data){
            var this_img_url = "https://app-upload.canberraport.com/user_post/"+data;

            //var a = document.getElementById('dl');
            //$('#dl').attr("href",this_img_url);
            //a.click();

            public_ct_create_all_done_or_not=1;//用于全部生成，代表这一次已完成
        },
        complete : function(XMLHttpRequest,status){}
    });
}

function get_base64_to_img_v2(v,this_time_loop_rsl){
    var id = randomNum(0,9999);
    console.log("正在下载长图地址:"+this_time_loop_rsl);

    //长图独有的请求地址
    $.ajax({
        url:'https://app-china.canberraport.com/_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            "f":"api_base64_image_content",
            "content":v,
            "path":"/mnt/cbrlife_app/_upload/changtu",
            "id":id
        },
        success:function(data){
            var this_img_url = "https://app-upload-china.canberraport.com/changtu/"+data;
            $('#ct_duobankuai_modal_download_rsl').append(this_time_loop_rsl+"&nbsp;<a href='#' onclick=\"download_pic_test('"+this_img_url+"')\" target='_blank'>下载</a><br>");
            $('#ct_preview_modal').modal('hide');
            $('#notice_alert').modal('hide');

            ct_grp_start++;
            ct_grp_doing_status=0;
        },
        complete : function(XMLHttpRequest,status){}
    });
}

function create_solo_changtu(id){
    $('#changtu_rsl_div_here_solo').show();
    $('#changtu_rsl_div_here_solo').html('');
    $('#changtu_rsl_img_here_solo').html('');
    $('body,html').animate(
        {
            scrollTop:0
        }
    );
    setTimeout("create_changtu_callback_solo("+id+")",500);
}

var timer1_solo;
function create_changtu_callback_solo(id){
    $('#notice_alert').modal('show');
    $('#notice_alert_text').html("<p align='center' style='color:red;'>正在处理长图<br>请稍后</p>");

    $('#ct_preview_modal_solo').modal('show');
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"create_changtu",//create_changtu
            id:id
        },
        success:function(data){
            //$('#'+id).hide();
            $('#changtu_rsl_div_here_solo').html(data);
            timer1_solo = setInterval("check_changtu_rsl_div_solo()",1000);//检测内容渲染结果
        },
        complete : function(XMLHttpRequest,status){}
    });
}

function output_canvas_solo(){
    console.log('开始输出canvas');
    html2canvas(document.querySelector("#changtu_rsl_div_here_solo"),{
        allowTaint : false,
        scrollX: 0,
        scrollY: -window.scrollY
    }).then(canvas => {
        $('#changtu_rsl_img_here_solo').html(canvas);
        $('#changtu_rsl_div_here_solo').hide();

        var canvas_png = canvas.toDataURL("image/png");
        //console.log(canvas_png);
        get_base64_to_img_v2_solo(canvas_png);
        //var a = document.createElement('a');
        // toDataURL defaults to png, so we need to request a jpeg, then convert for file download.
        //a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
        //a.download = 'changtu.jpg';
        //a.click();

    });
}

var ct_last_rsl_solo=0;
function check_changtu_rsl_div_solo(){
    var rsl = $('#changtu_rsl_div_here_solo').html().length;
    console.log("[solo]本次长图div渲染结果:"+rsl+",上一次结果:"+ct_last_rsl_solo);
    if(rsl==null||rsl==''||rsl==0){}else{

        if(rsl==ct_last_rsl_solo){
            ct_last_rsl_solo=0;
            clearInterval(timer1_solo);
            output_canvas_solo();
        }

    }
    ct_last_rsl_solo=rsl;
}

function get_base64_to_img_v2_solo(v){
    var id = randomNum(0,9999);
    $('#ct_preview_modal_solo_top_download_btn').html('');
    $.ajax({
        url:'https://app.canberraport.com/_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            "f":"api_base64_image_content",
            "content":v,
            "path":"/mnt/cbrlife_app/_upload/user_post",
            "id":id
        },
        success:function(data){
            var this_img_url = "../_upload/user_post/"+data;
            $('#ct_preview_modal_solo_top_download_btn').html("<a href='#' onclick=\"download_pic_test('"+this_img_url+"')\">下载</a><br>");
            $('#notice_alert').modal('hide');

        },
        complete : function(XMLHttpRequest,status){}
    });
}

function download_pic_test(url){
    window.open(url);
}

//生成从minNum到maxNum的随机数
function randomNum(minNum,maxNum){
    switch(arguments.length){
        case 1:
            return parseInt(Math.random()*minNum+1,10);
            break;
        case 2:
            return parseInt(Math.random()*(maxNum-minNum+1)+minNum,10);
            break;
        default:
            return 0;
            break;
    }
}

function ct_get_tab(){
    var uname = CookieUtil.get("uname");
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        dataType: 'json',
        data :{
            f:"ct_get_tab",
            uname:uname
        },
        success:function(data){
            if(data.done==="no"){
                $('#page_ct_output_nodata').html("<div style='margin-top:200px;text-align: center;color:#1B1B1B'>您没有权限浏览该页面</div>");
                $('#page_ct_output_nodata').show();
                $('#page_ct_output_list_main_div').hide();
                return;
            }else{
                $('#page_ct_output_nodata').hide();
                $('#page_ct_output_list_main_div').show();
            }

            var done_rsl = data.done;
            var no_done_rsl = data.no_done;

            var done_dom_rsl='';
            var no_done_dom_rsl='';

            for(var i=0;i<done_rsl.length;i++){
                var this_done_name = done_rsl[i]['name'];
                var this_done_count = done_rsl[i]['count'];
                var this_done_main_id = done_rsl[i]['main'];
                var this_done_sub_id = done_rsl[i]['sub'];
                var this_done_short = this_done_name + "<br>(" + this_done_count + ")";

                done_dom_rsl = done_dom_rsl + "<div class='col-3' onclick=\"ct_get_tab_type_list('done',"+this_done_main_id+","+this_done_sub_id+");\"><span><a href='#'>"+this_done_short+"</a></span></div>";
            }

            for(var z=0;z<no_done_rsl.length;z++){
                var this_no_done_name = no_done_rsl[z]['name'];
                var this_no_done_count = no_done_rsl[z]['count'];
                var this_no_done_main_id = no_done_rsl[z]['main'];
                var this_no_done_sub_id = no_done_rsl[z]['sub'];
                var this_no_done_short = this_no_done_name + "<br>(" + this_no_done_count + ")";
                no_done_dom_rsl = no_done_dom_rsl + "<div class='col-3' onclick=\"ct_get_tab_type_list('no_done',"+this_no_done_main_id+","+this_no_done_sub_id+");\"><span><a href='#'>"+this_no_done_short+"</a></span></div>";
            }

            $('#ct_tab_done_rsl').html(done_dom_rsl);
            $('#ct_tab_no_done_rsl').html(no_done_dom_rsl);
        },
        complete : function(XMLHttpRequest,status){}
    });
}

function ct_get_tab_type_list(type,main,sub){
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"ct_get_tab_type_list",
            type:type,
            main:main,
            sub:sub
        },
        success:function(data){
            if(type==="no_done"){
                $('#ct_no_done_list_rsl').html(data);
            }else{
                $('#ct_done_list_rsl').html(data);
            }

        },
        complete : function(XMLHttpRequest,status){}
    });
}

function copy_weixin_number(number_dom,button_dom){
    var weixin = $('#'+number_dom).html();
    weixin = trim(weixin);
    if(weixin===null||typeof(weixin)==='undefined'||weixin===''){
        show_notice_alert_modal("复制失败");
    }else{
        show_notice_alert_modal("已复制");
    }
    var clipboard = new ClipboardJS("."+button_dom, {
        text: function () {
            return weixin;
        }
    });
}

function copy_weixin_number_v2(wx,button_dom){
    var weixin = wx;
    weixin = trim(weixin);
    if(weixin===null||typeof(weixin)==='undefined'||weixin===''){
        show_notice_alert_modal("复制失败");
    }else{
        show_notice_alert_modal("已复制");
    }
    var clipboard = new ClipboardJS("."+button_dom, {
        text: function () {
            return weixin;
        }
    });
}

function trim(str){

    return str.replace(/(^\s*)|(\s*$)/g, "");

}

function show_notice_alert_modal(text){
    $('#notice_alert_text').html(text);
    $('#notice_alert').modal('show');
    setTimeout("hide_notice_alert_modal()",700);
}
function hide_notice_alert_modal(){
    $('#notice_alert').modal('hide');
}

function create_public_element_notice_alert(){
    $('body').append("<div class=\"modal fade\" id=\"notice_alert\" data-backdrop=\"static\" tabindex=\"-1\" style='z-index: 999999;' role=\"dialog\" aria-hidden=\"true\">\n" +
        "    <div  style='width: 150px;margin: 0 auto;' class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n" +
        "        <div class=\"modal-content\" style='width: 150px;'>\n" +
        "            <div class=\"modal-body\">\n" +
        "                <div class=\"text-center\">\n" +
        "                    <div>\n" +
        "                        <span id='notice_alert_text'></span>\n" +
        "                    </div>\n" +
        "<div style='text-align: center;'><button type=\"button\" class=\"btn\" data-dismiss=\"modal\" aria-label=\"Close\">\n" +
        "                    <span aria-hidden=\"true\">&times;</span>\n" +
        "                </button></div>" +
        "                </div>\n" +
        "            </div>\n" +
        "        </div>\n" +
        "    </div>\n" +
        "</div>");
}

function ct_copy_all_zhutiju(){
    var full_rsl='';
    $(".ct_all_weixin").each(function(){
        var this_rsl = $(this).html();
        full_rsl = full_rsl+this_rsl+"\n";
        console.log("本次主题句:"+full_rsl);
    });

    show_notice_alert_modal('已复制');
    var clipboard = new ClipboardJS(".copy_btn_all_zhutiju", {
        text: function () {
            return full_rsl;
        }
    });
}

var public_ct_create_all_done_or_not=0;
var this_all_id=[];
var public_ct_create_loop_start=0;
var timer2;
function ct_create_all_changtu(){
    public_ct_create_all_done_or_not=0;


    $('#notice_alert').modal('show');
    $('#notice_alert_text').html("<p align='center'>正在批量下载长图<br>请稍后</p>");

    //todo 调整这
    $(".ct_no_done_div").each(function(){
        var this_id = $(this).attr('id');
        this_all_id.push(this_id);
    });


    timer2 = setInterval("ct_create_all_check_and_go_to_next()",100);



}

function ct_create_all_check_and_go_to_next(){
    //get_base64_to_img(v)

    if(public_ct_create_loop_start==0){
        //代表第一个，直接执行
        console.log("loop第"+public_ct_create_loop_start+"次");

        var public_ct_create_all_count = public_ct_create_loop_start+1;
        $('#notice_alert_text').html("<p align='center'>正在批量下载长图<br>(第 "+public_ct_create_all_count+" 张)</p>");

        public_ct_create_all_done_or_not=0;
        var this_loop_id = this_all_id[public_ct_create_loop_start];
        console.log("这个item的id:"+this_loop_id);

        create_changtu(this_loop_id);

        public_ct_create_loop_start++;

    }

    if(public_ct_create_all_done_or_not==1){
        public_ct_create_all_done_or_not=0;
        $('#ct_preview_modal').modal('hide');

        console.log("loop第"+public_ct_create_loop_start+"次");

        var public_ct_create_all_count = public_ct_create_loop_start+1;
        $('#notice_alert_text').html("<p align='center'>正在批量下载长图<br>(第 "+public_ct_create_all_count+" 张)</p>");


        var this_loop_id = this_all_id[public_ct_create_loop_start];

        if(this_loop_id==null||this_loop_id==''||typeof(this_loop_id)=='undefined'||this_loop_id.length==0){
            clearInterval(timer2);
            $('#notice_alert_text').html("<p align='center' style='color:green;font-weight: bold;'>已全部下载完毕!</p>");
            setTimeout("hide_notice_alert_modal()",1000);
            return;
        }

        console.log("这个item的id:"+this_loop_id);
        create_changtu(this_loop_id);

        public_ct_create_loop_start++;
    }

}

function re_create_changtu(id,obj){
    $(obj).parent().parent()[0].remove();
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"re_create_changtu",
            id:id
        },
        success:function(data){
            ct_get_tab();
        },
        complete : function(XMLHttpRequest,status){}
    });
}

function page_is_get_infos(){
    var uname = CookieUtil.get("uname");
    $.ajax({
        url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
        timeout : 60000,
        type : 'post',
        data :{
            f:"page_is_get_infos",
            uname:uname,
            page:'tab-init-screen'
        },
        success:function(data){
            if(data==="no"){
                $('#page_is_output_nodata').html("<div style='margin-top:200px;text-align: center;color:#1B1B1B'>您没有权限浏览该页面</div>");
                $('#page_is_output_nodata').show();
                $('#page_is_output_list_main_div').hide();
                return;
            }

            $('#page_is_output_nodata').hide();
            $('#page_is_output_list_main_div').show();
            $('#page_is_output_list').html(data);
        },
        complete : function(XMLHttpRequest,status){}
    });
}

function delete_is_item(id){
    var msg = "您确定要删除该开屏广告吗?";
    if (confirm(msg)==true){
        $.ajax({
            url:PUBLIC_REMOTE_REQUEST_URL+'_c/_c',
            timeout : 60000,
            type : 'post',
            data :{
                f:"delete_is_item",
                id:id
            },
            success:function(data){
                alert("已删除");
                page_is_get_infos();
            },
            complete : function(XMLHttpRequest,status){}
        });
    }else{
        return false;
    }
}